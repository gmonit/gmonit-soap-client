package com.gmonit.soap.client;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.apache.cxf.binding.soap.saaj.SAAJOutInterceptor;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.wss4j.dom.WSConstants;
import org.apache.wss4j.dom.handler.WSHandlerConstants;

import com.gmonit.webservices.position.FaultDetailException;
import com.gmonit.webservices.position.GetPositionRequest;
import com.gmonit.webservices.position.GetPositionResponse;
import com.gmonit.webservices.position.Position;
import com.gmonit.webservices.position.Position_Service;
import com.gmonit.webservices.types.position.Lang;

public class GetPositionMain {
	
	public static final String DOMAIN = "example.com";
	public static final String KEYWORD = "example";
	public static final Lang LANGUAGE = Lang.EN;
	public static final GregorianCalendar DAY = new GregorianCalendar();
	static {
		DAY.set(2015, 0, 25);
	}
	
	public static final String YOUR_USERNAME_ON_GMONIT_COM = "username";
	public static final String YOUR_PASSWORD_ON_GMONIT_COM = "password";

	public static void main(String[] args) throws FaultDetailException, DatatypeConfigurationException {
		Position_Service service = new Position_Service();
		Position port = service.getPort(Position.class);

		Client client = ClientProxy.getClient(port);
		Endpoint cxfEndpoint = client.getEndpoint();

		// Authentication configuration
		Map<String, Object> outProps = new HashMap<String, Object>();
		WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
		cxfEndpoint.getOutInterceptors().add(wssOut);
		cxfEndpoint.getOutInterceptors().add(new SAAJOutInterceptor());
		outProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
		outProps.put(WSHandlerConstants.USER, YOUR_USERNAME_ON_GMONIT_COM);
		outProps.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
		outProps.put(WSHandlerConstants.PW_CALLBACK_CLASS, PasswordCallbackHandler.class.getName());
		
		// Creates request object and gets position.
		GetPositionRequest positionRequest = new GetPositionRequest();
		positionRequest.setDay(DatatypeFactory.newInstance().newXMLGregorianCalendar(DAY));
		positionRequest.setDomain(DOMAIN);
		positionRequest.setKeyword(KEYWORD);
		positionRequest.setLang(LANGUAGE);
		GetPositionResponse response = port.getPosition(positionRequest);
		System.out.println(response.getPosition().getPosition());
	}
}
