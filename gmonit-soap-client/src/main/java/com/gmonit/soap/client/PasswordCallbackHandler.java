package com.gmonit.soap.client;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.wss4j.common.ext.WSPasswordCallback;

public class PasswordCallbackHandler implements CallbackHandler {

	@SuppressWarnings("serial")
	private static final Map<String, String> PASSWORDS_STORAGE = new HashMap<String, String>() {
		{
			put(GetPositionMain.YOUR_USERNAME_ON_GMONIT_COM, GetPositionMain.YOUR_PASSWORD_ON_GMONIT_COM);
		}
	};

	/**
	 * @see javax.security.auth.callback.CallbackHandler#handle(javax.security.auth.callback.Callback[])
	 */
	@Override
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
		for (Callback callback : callbacks) {
			if (callback instanceof WSPasswordCallback) {
				String password = PASSWORDS_STORAGE.get(((WSPasswordCallback) callback).getIdentifier());
				if (password == null) {
					throw new IOException("Wrong username or password.");
				}
				((WSPasswordCallback) callback).setPassword(password);
			}
		}
	}
}
